﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ClockAnimator : MonoBehaviour
{
    public Transform hours, minutes, seconds;
    public bool showSeconds = false;
    public Color secondsColor;
    private const float hoursToDegress = 360f / 12f;
    private const float minutesToDegress = 360f / 60f;
    private const float secondsToDegress = 360f / 60f;

    private void Start()
    {
        // get a reference to the renderer component
        Renderer rend = seconds.transform.GetChild(0).GetComponent<Renderer>();
        // sets the color of the material to the color defined in the public variable
        rend.material.SetColor("_Color", secondsColor);
        if (showSeconds)
        {
            seconds.gameObject.SetActive(true);
        } else
        {
            seconds.gameObject.SetActive(false);
        }
    }

    private void Update()
    {
        TimeSpan time = DateTime.Now.TimeOfDay;
        hours.localRotation = Quaternion.Euler(0f,  (float)time.TotalHours * hoursToDegress, 0f);
        minutes.localRotation = Quaternion.Euler(0f,  (float)time.TotalMinutes * minutesToDegress, 0f);
        // if the showSeconds boolean is true, show the seconds hand and update it
        if (showSeconds)
        {
            seconds.localRotation = Quaternion.Euler(0f, (float)time.TotalSeconds * secondsToDegress, 0f);
        }
        
    }
}
